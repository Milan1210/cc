package com.cc.data_providers;

import org.testng.annotations.DataProvider;

import java.lang.reflect.Method;

public class InvalidLoginDataProvider {

    @DataProvider(name = "LoginData")
    public Object[][] loginData(Method m) {
        Object[][] obj = {new Object[1], new Object[2]};

        if (m.getName().contains("DataWithSpace")) {
            obj = new Object[1][2];

            obj[0][0] = "admin ";
            obj[0][1] = "admin ";
        }

        if (m.getName().contains("wrongEmail")) {
            obj = new Object[1][2];

            obj[0][0] = "admi";
            obj[0][1] = "admin";
        }

        if (m.getName().contains("wrongPassword")) {
            obj = new Object[1][2];

            obj[0][0] = "admin";
            obj[0][1] = "admi";
        }

        if (m.getName().contains("emptyEmail")) {
            obj = new Object[1][2];

            obj[0][0] = "";
            obj[0][1] = "admin";
        }

        if (m.getName().contains("emptyPassword")) {
            obj = new Object[1][2];

            obj[0][0] = "admin";
            obj[0][1] = "";
        }

        if (m.getName().contains("emptyEmailAndPassword")) {
            obj = new Object[1][2];

            obj[0][0] = "";
            obj[0][1] = "";
        }

        return obj;
    }
}
