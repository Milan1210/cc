package com.cc.data_providers;

import org.testng.annotations.DataProvider;

import java.lang.reflect.Method;

public class ValidLoginDataProvider {

    @DataProvider(name = "LoginData")
    public Object[][] loginData(Method m) {
        Object[][] obj = {new Object[1], new Object[2]};

        if (m.getName().contains("valid")) {
            obj = new Object[1][2];

            obj[0][0] = "admin";
            obj[0][1] = "admin";
        }
        return obj;
    }
}







