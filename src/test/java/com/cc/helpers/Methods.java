package com.cc.helpers;

import com.cc.DriverBase;
import com.cc.page_objects.pages.LoginPage;
import io.qameta.allure.Allure;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Methods {

    // public static void sendKeys(WebElement element, String text) {
    //     element.clear();
    //     element.sendKeys(text);
    // }

     public static void click(WebElement element) {
         Waiters.elementToAppear(element);
         element.click();
     }


    public static void logout() throws Exception {
        Allure.step("Click button 'Logout'");
        LoginPage loginPage = new LoginPage(DriverBase.driver);

//        loginPage.clickWithExecutor(loginPage.buttonLogout);
        loginPage.click(loginPage.buttonLogout);
    }

    public static String getPropertyValue(String key) throws IOException {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        InputStream resourceStream = loader.getResourceAsStream("src/test/resources/error_msg_create_new_entity.properties");
        Properties prop = new Properties();
        prop.load(resourceStream);
        return prop.getProperty(key);
    }


}
