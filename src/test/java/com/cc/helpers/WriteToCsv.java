package com.cc.helpers;

import java.io.FileWriter;
import java.io.IOException;

public class WriteToCsv {

    public void writeHeaderCsv() throws IOException {   //dodat da nalepi u fajl nove podatke

        FileWriter writer = new FileWriter("src/test/resources/test.csv");

        writer.append("Kundennummer");
        writer.append(';');
        writer.append("Nachname");
        writer.append(';');
        writer.append("Vorname");
        writer.append(';');
        writer.append("Geschlecht");
        writer.append(';');
        writer.append("Geburtsdatum");
        writer.append(';');
        writer.append("Alter");
        writer.append(';');
        writer.append("Alter bei Unterschrift");
        writer.append(';');
        writer.append("StraÃŸe");
        writer.append(';');
        writer.append("Hausnummer");
        writer.append(';');
        writer.append("Adresszusatz");
        writer.append(';');
        writer.append("PLZ");
        writer.append(';');
        writer.append("Ort");
        writer.append(';');
        writer.append("Land");
        writer.append(';');
        writer.append("Vertragsnummer");
        writer.append(';');
        writer.append("Studio");
        writer.append(';');
        writer.append("Vertragsmodell");
        writer.append(';');
        writer.append("Unterschriftsdatum");
        writer.append(';');
        writer.append("Trainingsbeginn");
        writer.append(';');
        writer.append("Vertragsbeginn");
        writer.append(';');
        writer.append("KÃ¼ndigungseingang");
        writer.append(';');
        writer.append("Vertragsende");
        writer.append(';');
        writer.append("KÃ¼ndigungsgrund");
        writer.append(';');
        writer.append("Vertragstyp");
        writer.append(';');
        writer.append("Vertragsstatus");
        writer.append(';');
        writer.append("Zahlmethode");  //25
        writer.append(';');
        writer.append("Zahlmodus");
        writer.append(';');
        writer.append("Aktueller Beitrag(Element)");
        writer.append(';');
        writer.append("letzte Inkassoabgabe");
        writer.append(';');
        writer.append("Summe InkassoÃ¼bergabe");
        writer.append(';');
        writer.append("Summe InkassoÃ¼bergabe noch offen");
        writer.append(';');
        writer.append("Telefonnummer");
        writer.append(';');
        writer.append("Mobilfunknummer"); //32
        writer.append(';');
        writer.append("E-Mail");
        writer.append(';');
        writer.append("Marketing via Telefon");
        writer.append(';');
        writer.append("Marketing via Mobiltelefon");
        writer.append(';');
        writer.append("Marketing via E-Mail");
        writer.append(';');
        writer.append("Marketing via Messenger");
        writer.append(';');
        writer.append("Marketing via Post");
        writer.append(';');
        writer.append("IBAN");
        writer.append(';');
        writer.append("Bankname");
        writer.append(';');
        writer.append("Kontoinhaber");
        writer.append(';');
        writer.append("Abweichender Kontoinhaber");
        writer.append(';');
        writer.append("Mandatsreferenz");
        writer.append(';');
        writer.append("SEPA-Mandat unterschrieben am");

        writer.flush();
        writer.close();

    }


    public void writeFirstRowCsv(int randomNum) throws IOException {   //dodat za test bez update-a contr nr 129548944
        FileWriter writer = new FileWriter("src/test/resources/test.csv", true);

        writer.append("\n");
        writer.append("AA0001_" + randomNum);
        writer.append(';');
        writer.append("Johnatans" + randomNum);
        writer.append(';');
        writer.append("Windrouz" + randomNum);
        writer.append(';');
        writer.append("maennlich");
        writer.append(';');
        writer.append("10.06.1901");
        writer.append(';');
        writer.append("118");
        writer.append(';');
        writer.append("118");
        writer.append(';');
        writer.append("Hochwiesenweg Strasse");
        writer.append(';');
        writer.append("0_");
        writer.append(';');
        writer.append("Address supplement 1");
        writer.append(';');
        writer.append("598");
        writer.append(';');
        writer.append("Muenchen");
        writer.append(';');
        writer.append("Deutchland");
        writer.append(';');
        writer.append("129548944");
        writer.append(';');
        writer.append("Studio Training_one");
        writer.append(';');
        writer.append("training fee_");
        writer.append(';');
        writer.append("05.06.2019");
        writer.append(';');
        writer.append("05.06.2019");
        writer.append(';');
        writer.append("01.06.2019");
        writer.append(';');
        writer.append("25.06.2019");
        writer.append(';');
        writer.append("10.01.2019");
        writer.append(';');
        writer.append("not applicable"); //22
        writer.append(';');
        writer.append("fit+ 24");
        writer.append(';');
        writer.append("active");
        writer.append(';');
        writer.append("null");
        writer.append(';');
        writer.append("null");
        writer.append(';');
        writer.append("null");
        writer.append(';');
        writer.append("41");
        writer.append(';');
        writer.append("null");
        writer.append(';');
        writer.append("null");
        writer.append(';');
        writer.append("null");
        writer.append(';');
        writer.append("null");
        writer.append(';');
        writer.append("johnys@gmail.com"); //33
        writer.append(';');
        writer.append("187977706792");
        writer.append(';');
        writer.append("ifnumln7o@mail.com");
        writer.append(';');
        writer.append("1");
        writer.append(';');
        writer.append("0");
        writer.append(';');
        writer.append("1");
        writer.append(';');
        writer.append("0");
        writer.append(';');
        writer.append("1");
        writer.append(';');
        writer.append("DE53014763789941290815");
        writer.append(';');
        writer.append("Bank of Deutchland");
        writer.append(';');
        writer.append("Schaebert Friedrich");
        writer.append(';');


        writer.flush();
        writer.close();

    }

    public void writeSecondRowCsvWithContractNumber500000000(int randomNum) throws IOException {   //dodat za update contr number 500000000
//        randomInt();
        FileWriter writer = new FileWriter("src/test/resources/test.csv", true);

        writer.append("\n");
        writer.append("AA0001_" + randomNum);
        writer.append(';');
        writer.append("Freenzz_" + randomNum);
        writer.append(';');
        writer.append("Schueschwarz_" + randomNum);
        writer.append(';');
        writer.append("maennlich");
        writer.append(';');
        writer.append("10.06.1901");
        writer.append(';');
        writer.append("118");
        writer.append(';');
        writer.append("118");
        writer.append(';');
        writer.append("Hochwiesenweg Strasse_");
        writer.append(';');
        writer.append("0_");
        writer.append(';');
        writer.append("Address supplement 0");
        writer.append(';');
        writer.append("18000");
        writer.append(';');
        writer.append("Muenchen_");
        writer.append(';');
        writer.append("Deutchland_");
        writer.append(';');
        writer.append("500000000");
        writer.append(';');
        writer.append("Studio Training_");
        writer.append(';');
        writer.append("training fee_");
        writer.append(';');
        writer.append("05.06.2019_");
        writer.append(';');
        writer.append("05.06.2019_");
        writer.append(';');
        writer.append("01.06.2019_");
        writer.append(';');
        writer.append("25.06.2019");
        writer.append(';');
        writer.append("10.01.2019");
        writer.append(';');
        writer.append("not applicable_"); //22
        writer.append(';');
        writer.append("fit+ 24_");
        writer.append(';');
        writer.append("active");
        writer.append(';');
        writer.append("null");
        writer.append(';');
        writer.append("null");
        writer.append(';');
        writer.append("null");
        writer.append(';');
        writer.append("41");
        writer.append(';');
        writer.append("null");
        writer.append(';');
        writer.append("null");
        writer.append(';');
        writer.append("null");
        writer.append(';');
        writer.append("null");
        writer.append(';');
        writer.append("784190232841"); //33
        writer.append(';');
        writer.append("187977706792");
        writer.append(';');
        writer.append("ifnumln7o@mail.com");
        writer.append(';');
        writer.append("1");
        writer.append(';');
        writer.append("0");
        writer.append(';');
        writer.append("1");
        writer.append(';');
        writer.append("0");
        writer.append(';');
        writer.append("1");
        writer.append(';');
        writer.append("DE53014763789941290815");
        writer.append(';');
        writer.append("Bank of Deutchland");
        writer.append(';');
        writer.append("Schaebert Friedrich");
        writer.append(';');


        writer.flush();
        writer.close();

    }

    public void writeToCSVs(int randomNum) throws IOException {   //dodat da nalepi u fajl nove podatke sa random novim contr numberom
//        randomInt();
        FileWriter writer = new FileWriter("src/test/resources/test.csv", true);
        writer.append("\n");
        writer.append("AA00021");
        writer.append(';');
        writer.append("Soniq_" + randomNum);
        writer.append(';');
        writer.append("Ilissinichesch_" + randomNum);
        writer.append(';');
        writer.append("maennlich");
        writer.append(';');
        writer.append("10.06.1901");
        writer.append(';');
        writer.append("118_");
        writer.append(';');
        writer.append("118");
        writer.append(';');
        writer.append("Hochwiesenweg Strasse_");
        writer.append(';');
        writer.append("0");
        writer.append(';');
        writer.append("Address supplement 0_");
        writer.append(';');
        writer.append("598_");
        writer.append(';');
        writer.append("Muenchen_");
        writer.append(';');
        writer.append("Deutchland");
        writer.append(';');
        writer.append("12_" + randomNum);
        writer.append(';');
        writer.append("Studio Training_two");
        writer.append(';');
        writer.append("training fee_");
        writer.append(';');
        writer.append("05.06.2019");
        writer.append(';');
        writer.append("05.06.2019");
        writer.append(';');
        writer.append("01.06.2019");
        writer.append(';');
        writer.append("25.06.2019");
        writer.append(';');
        writer.append("10.01.2019");
        writer.append(';');
        writer.append("not applicable");
        writer.append(';');
        writer.append("fit+ 24_");
        writer.append(';');
        writer.append("active_");
        writer.append(';');
        writer.append("null");  //25
        writer.append(';');
        writer.append("null");
        writer.append(';');
        writer.append("null");
        writer.append(';');
        writer.append("41");
        writer.append(';');
        writer.append("null");
        writer.append(';');
        writer.append("null");
        writer.append(';');
        writer.append("null");
        writer.append(';');
        writer.append("null");
        writer.append(';');
        writer.append("sonuq@gmail.com"); //33
        writer.append(';');
        writer.append("null");
        writer.append(';');
        writer.append("null");
        writer.append(';');
        writer.append("null");
        writer.append(';');
        writer.append("null");
        writer.append(';');
        writer.append("null");
        writer.append(';');
        writer.append("null");
        writer.append(';');
        writer.append("null");
        writer.append(';');
        writer.append("null");
        writer.append(';');
        writer.append("null");
        writer.append(';');
        writer.append("null");
        writer.append(';');


        writer.flush();
        writer.close();

    }

}
