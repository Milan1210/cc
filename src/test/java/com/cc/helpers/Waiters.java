package com.cc.helpers;

import com.cc.DriverBase;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Waiters extends DriverBase {

    public static WebDriverWait wait;

    public Waiters() throws Exception {
         wait = new WebDriverWait(driver, 60, 500);;

    }

    public static void sendKeys(WebElement element, String text) {
        wait.until(ExpectedConditions.elementToBeClickable(element)); //Can also elemnt to be visible
        element.clear();
        element.sendKeys(text);
    }

    public static void click(WebElement element) {
        wait.until(ExpectedConditions.elementToBeClickable(element));
        Waiters.elementToAppear(element);
        element.click();
    }

    public static void moveToElement(WebElement element){
        Actions action = new Actions(driver);
        Waiters.elementToAppear(element);
        action.moveToElement(element).perform();
    }

    public static void scrollToElement(WebElement element) {
        Waiters.elementToAppear(element);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView();", element);

    }

    public static void elementToAppear(WebElement element) {
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public static void elementToAppear(By locator) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public static void clickWithExecutor(WebElement element) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].click()", element);

    }

    public static void invisibilityOfElementLocated(By locator) {
        wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
    }

//    Waiting for load to dissapear
    public static void invisibilityOfLoader(){
        invisibilityOfElementLocated(By.cssSelector("div.mc-loader-wrapper.mc-loader-show"));
    }



}
