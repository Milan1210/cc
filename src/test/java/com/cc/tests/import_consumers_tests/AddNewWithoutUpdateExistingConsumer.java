package com.cc.tests.import_consumers_tests;
import com.cc.DriverBase;
import com.cc.helpers.Log;
import com.cc.helpers.Methods;
import com.cc.helpers.Waiters;
import com.cc.listeners.TestListener;
import com.cc.page_objects.pages.ImportConsumersPage;
import com.cc.page_objects.pages.LoginPage;
import io.qameta.allure.Allure;
import io.qameta.allure.Description;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.Random;

@Listeners(TestListener.class)
public class AddNewWithoutUpdateExistingConsumer extends DriverBase {
    public int randomNum;
    public String externalId;
    public String firstName;
    public String lastName;
    public String contractNumber;
    public int valueOfTotalElementsInTheTableBeforeImport;

    public void addRandomNumToValues() {
        Random r = new Random();
        int low = 10;
        int high = 10000000;
        randomNum = r.nextInt(high - low) + low;
        String randomValue = Integer.toString(randomNum);
        externalId = "AA0001_".concat(randomValue);
        firstName = "Freenzz_".concat(randomValue);
        lastName = "Schueschwarz_".concat(randomValue);
        contractNumber = "12_".concat(randomValue);
    }

    @BeforeMethod
    public void beforeMethod() throws Exception {
        Log.info("I am in Login method ");
        Allure.step("Login with admin credentials");
        LoginPage loginPage = new LoginPage(driver);
        loginPage.loginMethod("admin", "admin");
    }


// TODO: connect to Jenkins


    @Test(priority = 1, description = "Add new consumer without updating existing one")
    @Description("Add new consumer without updating existing, check if new one is added and if existing isn't updated")
    @Story("ADD NEW  CONSUMER WITHOUT UPDATING AN EXISTING ONE")
    public void testImportModeAddWithoutUpdate() throws Exception {
//        Allure.getLifecycle().updateTestCase(testResult -> testResult.setHistoryId(null));
//        Allure.getLifecycle().updateTestCase(testResult -> testResult.setName("Import consumers " + DriverFactory.browser));

        ImportConsumersPage importConsumersPage = new ImportConsumersPage(driver);

        importConsumersPage.clickConsumersOverviewToLoadTable();
        Allure.step("Click Consumers overview in side navigation bar");
        Waiters.invisibilityOfLoader();

        String elementsInTheTableBeforeImport = importConsumersPage.getNumberOfTotalElementsInTheTable();
        valueOfTotalElementsInTheTableBeforeImport = Integer.parseInt(elementsInTheTableBeforeImport);

        addRandomNumToValues();

        importConsumersPage.writeToCsvExistingAndNewConsumer(randomNum);
        Allure.step("Write to csv file different name for existing consumer and write one new consumer");
        Allure.step("Click 'Import consumers'");

        importConsumersPage.clickImportConsumersAndUploadFile("ADD"); //da izabere Add WITHOUT update
        Allure.step("Upload csv file and choose import mode 'Add new without update existing consumer' ");

        Thread.sleep(7000);

        String elementsInTheTableAfterImport = importConsumersPage.getNumberOfTotalElementsInTheTable();
        int afterImport = Integer.parseInt(elementsInTheTableAfterImport);

        importConsumersPage.getRowsAndColumnsFromTableConsumers("129548944"); //added method to print all rows and columns

        Allure.step("Verify that existing consumer isn't updated and that new consumer is added");

        Allure.step("Added consumer is: " + firstName + " " + lastName + ", with contract number: " + contractNumber);

        try {
            Assert.assertEquals(importConsumersPage.columnNameValueText, "Johnatans Windrouz");
        } catch (AssertionError ae) {
            throw new AssertionError("Consumer is updated, but shouldn't be");
        }
        try {
            Assert.assertEquals(afterImport - 1, valueOfTotalElementsInTheTableBeforeImport); //check if one new cons is added
        } catch (AssertionError assertionError) {
            throw new AssertionError("Pre importa broj consumera u tabeli: " + elementsInTheTableBeforeImport +
                    ", posle importa broj cons u tabeli je " + elementsInTheTableAfterImport); //uporedjujemo broj pre i posle importa
        }

        Methods.logout();
    }

}


