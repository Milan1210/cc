package com.cc.tests.import_consumers_tests;

import com.cc.DriverBase;
import com.cc.helpers.Log;
import com.cc.helpers.Waiters;
import com.cc.listeners.TestListener;
import com.cc.page_objects.pages.ImportConsumersPage;
import com.cc.page_objects.pages.LoginPage;
import io.qameta.allure.Allure;
import io.qameta.allure.Description;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.Random;

@Listeners(TestListener.class)
public class AddNewAndUpdateExistingConsumer extends DriverBase {
    public int randomNum;
    public String externalId;
    public String firstName;
    public String lastName;
    public String contractNumber;

    public void addRandomNumToValues() {
        Random r = new Random();
        int low = 10;
        int high = 10000000;
        randomNum = r.nextInt(high - low) + low;
        String randomValue = Integer.toString(randomNum);
        externalId = "AA0001_".concat(randomValue);
        firstName = "Freenzz_".concat(randomValue);
        lastName = "Schueschwarz_".concat(randomValue);
        contractNumber = "12_".concat(randomValue);
    }

    @BeforeMethod
    public void beforeMethod() throws Exception {
        Log.info("I am in Login method ");
        Allure.step("Login with admin credentials");
        LoginPage loginPage = new LoginPage(driver);
        loginPage.loginMethod("admin", "admin");
    }


// TODO: make suite
// TODO: connect to Jenkins

    @Test(priority = 2, description = "Add new consumer and update existing consumer")
    @Description("Add new consumer and update existing consumer by adding random number into firstName and lastName.")
    @Story("ADD NEW CONSUMER AND UPDATE EXISTING CONSUMER")
    public void testAddNewAndUpdateExistingConsumerAndCheckGender() throws Exception {
        ImportConsumersPage importConsumersPage = new ImportConsumersPage(driver);
        importConsumersPage.clickConsumersOverviewToLoadTable();
        Allure.step("Click Consumers overview in side navigation bar");
        Waiters.invisibilityOfLoader();
        String elementsInTheTableBeforeImport = importConsumersPage.getNumberOfTotalElementsInTheTable();
        int valueOfTotalElementsInTheTableBeforeImport = Integer.parseInt(elementsInTheTableBeforeImport);

        addRandomNumToValues();

        importConsumersPage.writeExistingAndNewConsumerToCsv(randomNum); //kada je test za UPDATE i za ADD I UPDATE

        importConsumersPage.clickImportConsumersAndUploadFile("ADDUPD");//da izabere ADD and UPDATE
        Allure.step("Click 'Import consumers'");
        Allure.step("Upload csv file and choose mapping");
        Allure.step("Choose import mode 'Add new and update existing consumer'");
        Allure.step("Click 'Import'");

        Thread.sleep(7000);

        importConsumersPage.clickConsumersOverviewToLoadTable();
        String elementsInTheTableAfterImport = importConsumersPage.getNumberOfTotalElementsInTheTable();
        int afterImport = Integer.parseInt(elementsInTheTableAfterImport);

        importConsumersPage.getRowsAndColumnsFromTableConsumers("500000000");
        Thread.sleep(2000);

        try {
            System.out.println("curent consumer is:"+importConsumersPage.columnNameValueText+" our consumer is: "+firstName + " " + lastName);
            Assert.assertEquals(importConsumersPage.columnNameValueText, firstName + " " + lastName);//update pomocu random broja u imenu i prezimenu
        } catch (AssertionError ae) {
            throw new AssertionError("Consumer isn't updated, but should be");
        }

        Allure.step("Verify that new consumer is added and that existing consumer is updated ");

        Allure.step("Updated consumer is: " + firstName + " " + lastName + ", with contract number: 500000000");

        Allure.step("Added consumer is: Soniq_" + randomNum + " " + "Ilissinichesch_" + randomNum + ", with contract number: " + contractNumber);

        try {
            Assert.assertEquals(afterImport - 1, valueOfTotalElementsInTheTableBeforeImport);
        } catch (AssertionError assertionError) {
            throw new AssertionError("Number of consumers should be: " + elementsInTheTableBeforeImport +
                    ", but after import the number is different: " + elementsInTheTableAfterImport);
        }

        importConsumersPage.openViewModal(contractNumber);
        try {
            Assert.assertTrue(importConsumersPage.checkGender());
        } catch (AssertionError assertionError) {
            throw new AssertionError("Gender is NULL or it doesn't have expected value");
        }
//        Methods.click(importConsumersPage.buttonXonGenderModal); //dodato da klikne na z da zatvori modal View consumer
//        Methods.logout();
    }

}


