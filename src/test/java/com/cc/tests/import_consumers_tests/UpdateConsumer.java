package com.cc.tests.import_consumers_tests;

import com.cc.DriverBase;
import com.cc.helpers.Log;
import com.cc.helpers.Methods;
import com.cc.helpers.Waiters;
import com.cc.listeners.TestListener;
import com.cc.page_objects.pages.ImportConsumersPage;
import com.cc.page_objects.pages.LoginPage;
import io.qameta.allure.Allure;
import io.qameta.allure.Description;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.Random;

@Listeners(TestListener.class)
public class UpdateConsumer extends DriverBase {
    public int randomNum;
    public String externalId;
    public String firstName;
    public String lastName;
    public String contractNumber;

    public void addRandomNumToValues() {
        Random r = new Random();
        int low = 10;
        int high = 10000000;
        randomNum = r.nextInt(high - low) + low;
        String randomValue = Integer.toString(randomNum);
        externalId = "AA0001_".concat(randomValue);
        firstName = "Freenzz_".concat(randomValue);
        lastName = "Schueschwarz_".concat(randomValue);
        contractNumber = "12_".concat(randomValue);
    }

    @BeforeMethod
    public void beforeMethod() throws Exception {
        Log.info("I am in Login method ");
        Allure.step("Login with admin credentials");
        LoginPage loginPage = new LoginPage(driver);
        loginPage.loginMethod("admin", "admin");
    }


// TODO: connect to Jenkins

    @Test(priority = 1, description = "Update existing consumer ")
    @Description("Update existing consumer by adding random number into firstName and lastName, check if new one isn't added")
    @Story("UPDATE EXISTING CONSUMER")
    public void testUpdateExistingConsumerWithContractNumber500000000() throws Exception {
        ImportConsumersPage importConsumersPage = new ImportConsumersPage(driver);
        importConsumersPage.clickConsumersOverviewToLoadTable();
        Allure.step("Click Consumers overview in side navigation bar");
        Waiters.invisibilityOfLoader();

        String elementsInTheTableBeforeImport = importConsumersPage.getNumberOfTotalElementsInTheTable();

        addRandomNumToValues();

        importConsumersPage.writeExistingAndNewConsumerToCsv(randomNum);//kada je test da update-uje

        importConsumersPage.clickImportConsumersAndUploadFile("UPD"); //da izabere UPDATE
        Allure.step("Click 'Import consumers'");
        Allure.step("Upload csv file and choose mapping");
        Allure.step("Choose import mode 'Update existing consumer'");
        Allure.step("Click 'Import' ");

        Thread.sleep(7000);

        importConsumersPage.clickConsumersOverviewToLoadTable();
        String elementsInTheTableAfterImport = importConsumersPage.getNumberOfTotalElementsInTheTable();

        importConsumersPage.getRowsAndColumnsFromTableConsumers("500000000");

        try {
            Assert.assertEquals(importConsumersPage.columnNameValueText, firstName + " " + lastName);
        } catch (AssertionError ae) {
            throw new AssertionError("Consumer isn't updated, but should be");
        }
        Allure.step("Verify that existing consumer is updated and that new consumer isn't added");
        Allure.step("Updated consumer is: " + firstName + " " + lastName + ", with contract number: 500000000");

        try {
            Assert.assertEquals(elementsInTheTableBeforeImport, elementsInTheTableAfterImport); //check if new consumer isn't added
        } catch (AssertionError assertionError) {
            throw new AssertionError("Number of consumers should be: " + elementsInTheTableBeforeImport +
                    "but after import the nubmer is different: " + elementsInTheTableAfterImport);
        }

        Methods.logout();
    }


}


