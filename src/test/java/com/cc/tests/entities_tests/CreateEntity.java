package com.cc.tests.entities_tests;

import com.cc.DriverBase;
import com.cc.helpers.Methods;
import com.cc.page_objects.general_elements.SideNavBar;
import com.cc.page_objects.general_elements.Table;
import com.cc.page_objects.pages.Entities;
import com.cc.page_objects.pages.LoginPage;
import io.qameta.allure.Allure;
import io.qameta.allure.Description;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


public class CreateEntity extends DriverBase {

    @BeforeMethod
    public void beforeMethod() throws Exception {
        Allure.step("Login with admin credentials");
        LoginPage loginPage = new LoginPage(driver);
        loginPage.loginMethod("admin", "NoviAdmin!2020");
    }

    @Description("Create new entity when required fields are filled if chosen true, and all fields filled if chosen false")
    @Test()
    public void createNewEntity() throws Exception {
//        Allure.getLifecycle().updateTestCase(testResult -> testResult.setHistoryId(null));
//        Allure.getLifecycle().updateTestCase(testResult -> testResult.setName("Create new entity " + DriverFactory.browser));

        Allure.step("Wait till dashboard loaded");
        SideNavBar sideNavBar = new SideNavBar(driver);

        Allure.step("Go to the module 'Entities'");
        sideNavBar.getEntities();

        Entities entities = new Entities(driver);

        Table table = new Table(driver);

        //number of entities in table before creation
        entities.numberOfEntities = table.totalDisplayedElements();

        Allure.step("Click button 'Create new entity' ");
        Methods.click(entities.createEntityBtn);

        Allure.step("Fill mandatory fields and click next button");
        Allure.step("Click button 'Next'");
        Allure.step("Click button 'Next'");

       // entities.filledFields(true);  //only required fields filled

        entities.filledFields(true); //all fields filled

        entities.clickNextAndCreateEntity();  //insertField method has renamed

//        Methods.logout();
    }


}


