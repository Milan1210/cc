package com.cc.tests.login_tests;

import com.cc.DriverBase;
import com.cc.data_providers.ValidLoginDataProvider;
import com.cc.helpers.Methods;
import com.cc.page_objects.pages.LoginPage;
import io.qameta.allure.Allure;
import io.qameta.allure.Description;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ValidLogin extends DriverBase {

    @Description("Valid login as admin")
    @Test(priority = 0, dataProviderClass = ValidLoginDataProvider.class, dataProvider = "LoginData")
    public void validLoginAdmin(String email, String password) throws Exception {

//        Allure.getLifecycle().updateTestCase(testResult -> testResult.setHistoryId(null));
//        Allure.getLifecycle().updateTestCase(testResult -> testResult.setName("Admin login " + DriverFactory.browser));

        LoginPage loginPage = new LoginPage(driver);
        loginPage.loginMethod(email, password);
                

        Allure.step("Wait for logout button to appear...");
        loginPage.elementToAppear(loginPage.buttonLogout);
        try {
            Assert.assertTrue(loginPage.buttonLogout.isDisplayed());
        } catch (AssertionError e) {
            throw new AssertionError("Logout button is not displayed or you cannot login");
        }
        Methods.logout();




    }
}