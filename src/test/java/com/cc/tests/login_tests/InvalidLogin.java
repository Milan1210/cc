package com.cc.tests.login_tests;

import com.cc.DriverBase;
import com.cc.data_providers.InvalidLoginDataProvider;
import com.cc.page_objects.pages.LoginPage;
import io.qameta.allure.Allure;
import io.qameta.allure.Description;
import org.openqa.selenium.NoSuchElementException;
import org.testng.Assert;
import org.testng.annotations.Test;

public class InvalidLogin extends DriverBase {

    public static final String WARNING_MSG = "Bad credentials";
    public static final String BLOCK_MSG = "Logging in is temporary unavailable";

    @Description("Login as admin with space and password admin witth space")
    @Test(priority = 0, dataProviderClass = InvalidLoginDataProvider.class, dataProvider = "LoginData")
    public void DataWithSpace(String email, String password) throws Exception {

//        Allure.getLifecycle().updateTestCase(testResult -> testResult.setHistoryId(null));
//        Allure.getLifecycle().updateTestCase(testResult -> testResult.setName("Invalid login " + DriverFactory.browser));


        LoginPage loginPage = new LoginPage(driver);
        loginPage
                .loginMethod(email, password);

        loginPage.elementToAppear(loginPage.messageBadCredentials);
        Allure.step("Wait for warning message to appear...");
        try {
            Assert.assertEquals(WARNING_MSG, loginPage.messageBadCredentials.getText());
            Allure.step(WARNING_MSG + " appeared!");
        } catch (NoSuchElementException e) {
            throw new Exception("Warning message is not displayed.");
        }
    }

    @Description("Login with wrong email and valid password")
    @Test(priority = 1, dataProviderClass = InvalidLoginDataProvider.class, dataProvider = "LoginData")
    public void wrongEmail(String email, String password) throws Exception {

//        Allure.getLifecycle().updateTestCase(testResult -> testResult.setHistoryId(null));
//        Allure.getLifecycle().updateTestCase(testResult -> testResult.setName("Invalid login " + DriverFactory.browser));

        LoginPage loginPage = new LoginPage(driver);
        loginPage
                .loginMethod(email, password);

        loginPage.elementToAppear(loginPage.messageBadCredentials);
        Allure.step("Wait for warning message to appear...");
        try {
            Assert.assertEquals(WARNING_MSG, loginPage.messageBadCredentials.getText());
            Allure.step(WARNING_MSG + " appeared!");
        } catch (NoSuchElementException e) {
            throw new Exception("Warning message is not displayed.");
        }
    }

    @Description("Login with empty field email and valid password")
    @Test(priority = 2, dataProviderClass = InvalidLoginDataProvider.class, dataProvider = "LoginData")
    public void emptyEmail(String email, String password) throws Exception {

//        Allure.getLifecycle().updateTestCase(testResult -> testResult.setHistoryId(null));
//        Allure.getLifecycle().updateTestCase(testResult -> testResult.setName("Invalid login " + DriverFactory.browser));

        LoginPage loginPage = new LoginPage(driver);
        loginPage
                .loginMethodWithoutClick(email, password);

        Allure.step("Wait for login button to become enabled...");
        try {
            Assert.assertFalse(loginPage.buttonLogin.isEnabled());
            Allure.step("Login button is not enabled");
        } catch (AssertionError e) {
            throw new Exception("Login button is not disabled.");
        }
    }

    @Description("Login with empty field email and empty field password")
    @Test(priority = 3, dataProviderClass = InvalidLoginDataProvider.class, dataProvider = "LoginData")
    public void emptyEmailAndPassword(String email, String password) throws Exception {
        driver.navigate().refresh();
//        Allure.getLifecycle().updateTestCase(testResult -> testResult.setHistoryId(null));
//        Allure.getLifecycle().updateTestCase(testResult -> testResult.setName("Invalid login " + DriverFactory.browser));

        LoginPage loginPage = new LoginPage(driver);
        loginPage
                .loginMethodWithoutClick(email, password);

        Allure.step("Wait for login button to become enabled...");
        try {
            Assert.assertFalse(loginPage.buttonLogin.isEnabled());
            Allure.step("Login button is not enabled");
        } catch (AssertionError e) {
            throw new Exception("Login button is not disabled.");
        }
    }

    @Description("Login with valid email and empty password field")
    @Test(priority = 4, dataProviderClass = InvalidLoginDataProvider.class, dataProvider = "LoginData")
    public void emptyPassword(String email, String password) throws Exception {
        driver.navigate().refresh();
//        Allure.getLifecycle().updateTestCase(testResult -> testResult.setHistoryId(null));
//        Allure.getLifecycle().updateTestCase(testResult -> testResult.setName("Invalid login " + DriverFactory.browser));
        LoginPage loginPage = new LoginPage(driver);
        loginPage
                .loginMethodWithoutClick(email, password);

        Allure.step("Wait for login button to become enabled...");
        try {
            Assert.assertFalse(loginPage.buttonLogin.isEnabled());
            Allure.step("Login button is not enabled");
        } catch (AssertionError e) {
            throw new Exception("Login button is not disabled.");
        }
    }


    @Description("Login with valid email and wrong password")
    @Test(priority = 5, dataProviderClass = InvalidLoginDataProvider.class, dataProvider = "LoginData")
    public void wrongPassword(String email, String password) throws Exception {

//        Allure.getLifecycle().updateTestCase(testResult -> testResult.setHistoryId(null));
//        Allure.getLifecycle().updateTestCase(testResult -> testResult.setName("Invalid login " + DriverFactory.browser));

        LoginPage loginPage = new LoginPage(driver);
        loginPage
                .loginMethod(email, password);
        loginPage.elementToAppear(loginPage.messageLoginLocked);
        Allure.step("Wait for warning message to appear...");
        try {
            Assert.assertEquals(BLOCK_MSG, loginPage.messageLoginLocked.getText());
            Allure.step(BLOCK_MSG + " appeared!");
        } catch (NoSuchElementException e) {
            throw new Exception("Warning message is not displayed.");
        }
    }

}
