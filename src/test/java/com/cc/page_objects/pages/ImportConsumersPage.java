package com.cc.page_objects.pages;

import com.cc.helpers.Methods;
import com.cc.helpers.Waiters;
import com.cc.helpers.WriteToCsv;
import com.cc.page_objects.general_elements.SideNavBar;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class ImportConsumersPage extends Waiters {

    public ImportConsumersPage(WebDriver driver) throws Exception {
        super();
        PageFactory.initElements(driver, this);
    }

    final File file = new File("src/test/resources/test.csv"); //for TEST 1 consumer
    final String path = file.getAbsolutePath();


    @FindBy(css = "button.mc-link.pl-3")
    public WebElement buttonImportConsumer;

    @FindBy(css = "input#importFile")
    public WebElement chooseFileToUpload;

    @FindBy(css = "button.btn.btn-primary.mr-3")
    public WebElement buttonUpload;

    @FindBy(css = "button.pr-3.ml-auto.btn.btn-outline-primary")
    public WebElement buttonChooseMapping;

    @FindBy(id = "fi-templateMapping-Mapping - TEST")
    public WebElement radioButtonOfChosenMapping;

//    @FindBy(xpath = "//*[@id=\"mcc-fi-templateMapping\"]/form/ul/li[16]/label")
//    public WebElement chosenMappingTESTForDevEnv;

    @FindBy(xpath = "//*[@id=\"mcc-fi-templateMapping\"]/form/ul/li[3]/label")
    public WebElement chosenMappingAutEnv;

    @FindBy(css = "button.btn.btn-primary.mr-3")
    public WebElement buttonApplyMapping;

    @FindBy(id = "mcc-fi-importMode")
    public WebElement selectImportMode;

    @FindBy(css = "button.btn.btn-primary.mr-3")
    public WebElement buttonImport;

    @FindBy(css = "button.btn.btn-secondary.move-right")
    public WebElement buttonCancel;

    @FindBy(css = "tbody.mc-table-body")
    public WebElement tableConsumers;

    @FindBy(xpath = "//mcc-pagination/div/div[2]/p[3]")
    public WebElement totalElementsInTheTable;

    @FindBy(id = "mcc-fi-txbSearchTerm")
    public WebElement searchBox;

    @FindBy(xpath = "//div[text()='Gender:']/following-sibling::div")
    public WebElement genderModal;

    @FindBy(xpath = "//button/span")
    public WebElement buttonXonGenderModal;

    final SideNavBar sideNavBar = new SideNavBar(driver);

    public String columnNameValueText;

    public String getNumberOfTotalElementsInTheTable() {
        elementToAppear(totalElementsInTheTable);
        return totalElementsInTheTable.getText();
    }

    public void clickButtonImportConsumer() throws InterruptedException {
        invisibilityOfLoader();
        Thread.sleep(3000);
        Methods.click(buttonImportConsumer);

    }

    public void uploadChosenFile() throws InterruptedException {
        Thread.sleep(5000);
        chooseFileToUpload.sendKeys(path);

    }

    public void clickImportConsumersAndUploadFile(String importModeValue) throws Exception { //import mode Add without update need to assert Total el
        elementToAppear(buttonImportConsumer);
        clickButtonImportConsumer();
        elementToAppear(buttonUpload);
        uploadChosenFile();
        Methods.click(buttonUpload);
        Thread.sleep(5000);
//        click(buttonChooseMapping);
//        Methods.click(buttonCancel);
        Methods.click(buttonChooseMapping);
        Methods.click(chosenMappingAutEnv);
        Methods.click(buttonApplyMapping);
        elementToAppear(selectImportMode);
        chooseImportModeAddNewConsumerWithoutUpdatingExisting(importModeValue);
        Methods.click(buttonImport);

    }

    public void clickConsumersOverviewToLoadTable() {
        sideNavBar.clickConsumersOverviewInSideBar();
        invisibilityOfLoader();
        elementToAppear(buttonImportConsumer);
    }

    public void chooseImportModeAddNewConsumerWithoutUpdatingExisting(String importModeValue) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView();", selectImportMode);
        Select importMode = new Select(selectImportMode);
        importMode.selectByValue(importModeValue);
    }

    public void writeToCsvExistingAndNewConsumer(int num) throws IOException { //za test WITHOUT update
        WriteToCsv wtc = new WriteToCsv();
        wtc.writeHeaderCsv();
        wtc.writeToCSVs(num);
        wtc.writeFirstRowCsv(num);
    }

    public void writeExistingAndNewConsumerToCsv(int num) throws IOException {
        WriteToCsv wtc = new WriteToCsv();
        wtc.writeHeaderCsv();
        wtc.writeToCSVs(num);
        wtc.writeSecondRowCsvWithContractNumber500000000(num);
    }

    public void getRowsAndColumnsFromTableConsumers(String contractNumber) {
        List<WebElement> rows_table = tableConsumers.findElements(By.tagName("tr"));
        int rows_count = rows_table.size();

        for (WebElement element : rows_table) {

            //To locate columns(cells) of that specific row.
            List<WebElement> Columns_row = element.findElements(By.tagName("td"));

            //To calculate number of columns (cells). In that specific row.
            int columns_count = Columns_row.size();

            //Loop will execute till the last cell of that specific row.
            for (int column = 0; column < columns_count; column++) {
            }

            if (Columns_row.get(4).getText().equals(contractNumber)) {
                columnNameValueText = Columns_row.get(2).getText();

            }
        }
    }

    public void openViewModal(String contractNumber) {
        searchBox.sendKeys(contractNumber, Keys.ENTER);
        invisibilityOfLoader();
        String actionButton = driver.findElement(By.xpath("//table/tbody/tr/td[3]")).getText();
        System.out.println("search name is: " + actionButton);
        WebElement actionButtonN = driver.findElement(By.xpath("//table/tbody/tr/td[2]//ul/li/button"));
        clickWithExecutor(actionButtonN);

    }

    public boolean checkGender() {
        String gender = genderModal.getText();
        return gender.equals("MALE") || gender.equals("FEMALE") || gender.equals("OTHER");
    }

}






