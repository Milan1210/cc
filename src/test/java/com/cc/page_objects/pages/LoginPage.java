package com.cc.page_objects.pages;

import com.cc.helpers.Waiters;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
public class LoginPage extends Waiters {

    public LoginPage(WebDriver driver) throws Exception {
        super();
        PageFactory.initElements(driver, this);
    }
    //elements
    @FindBy(id = "username")
    public  WebElement textboxEmail;
    @FindBy(id = "password")
    public  WebElement textboxPassword;
    @FindBy(id = "loginBtn")
    public WebElement buttonLogin;
    @FindBy(css = "ul > li.mc-header-right-action.mc-header-logout > a")
    public WebElement buttonLogout;
    @FindBy(className = "mc-notify-message-wrapper")
    public WebElement messageBadCredentials;
    @FindBy(xpath = "//div/h4[@class='pb-4']")
    public WebElement messageLoginLocked;
    @FindBy(id = "pieChart1")
    public WebElement graphiconConsuemrs;


    public  LoginPage loginMethod(String email, String password){
        sendKeys(textboxEmail, email);
        sendKeys(textboxPassword, password);
        click(buttonLogin);
        elementToAppear(graphiconConsuemrs);
        return this;
    }

    public  LoginPage loginMethodWithoutClick(String email, String password){
        sendKeys(textboxEmail, email);
        sendKeys(textboxPassword, password);
        return this;
    }


}