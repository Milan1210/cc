package com.cc.page_objects.pages;

import com.cc.helpers.Methods;
import com.cc.helpers.Waiters;
import com.cc.page_objects.general_elements.Table;
import io.qameta.allure.Allure;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.io.File;


public class Entities extends Waiters {

    //constructor
    public Entities(WebDriver driver) throws Exception {
        super();
        PageFactory.initElements(driver, this);
    }

    //web domain and name must be unique
    private static final String DOMAIN = RandomStringUtils.randomAlphabetic(4).toLowerCase() + ".com";
    private static final String NAME = RandomStringUtils.randomAlphabetic(4);
    private static final String ABBREVIATION = RandomStringUtils.randomAlphabetic(4);
    private static final String DESCRIPTION = RandomStringUtils.randomAlphabetic(4).toLowerCase();
    private static final String IP_WHITELIST = ("123.123.123.123");
    private static final String PHONE_NUMBER = RandomStringUtils.randomNumeric(4);
    private static final String VAT_NUMBER = RandomStringUtils.randomNumeric(4).toLowerCase();
    private static final String FAX_NUMBER = RandomStringUtils.randomNumeric(4);
    private static final String IMPRESSUM = RandomStringUtils.randomAlphabetic(4);
    private static final String ADDRESS = RandomStringUtils.randomAlphabetic(4).toLowerCase();
    private static final String LEGAL_REPRESENTATIVE_EMAIL = RandomStringUtils.randomAlphabetic(5).toLowerCase() + "@gmail.com";
    private static final String LEGAL_REPRESENTATIVE = RandomStringUtils.randomAlphabetic(4);
    private static final String EXTERNAL_CREDITOR_ID = RandomStringUtils.randomNumeric(5);


    //Absolute/relative path to image for upload
    final File file = new File("src/test/resources/pictureForUpload.jpg");
    final String path = file.getAbsolutePath();

    //elements
    @FindBy(id = "mc-create-new-entity-btn")
    public WebElement createEntityBtn;

    @FindBy(css = "input.hidden-input")
    private WebElement uploadImageLnk;

    // iput fields
    @FindBy(id = "name")
    private WebElement nameInput;

    @FindBy(id = "abbreviation")
    private WebElement abbreviationInput;

    @FindBy(id = "mail")
    private WebElement mailDomainInput;

    @FindBy(id = "webDomain")
    private WebElement webDomainInput;

    @FindBy(id = "description")
    private WebElement descriptionInput;

    @FindBy(id = "ip")
    private WebElement ipWhitelistInput;

    @FindBy(id = "phoneNumber")
    private WebElement phoneNumberInput;

    @FindBy(id = "vatNumber")
    private WebElement vatNumberInput;

    @FindBy(id = "faxNumber")
    private WebElement faxNumberInput;

    @FindBy(id = "impressum")
    private WebElement impressumInput;

    @FindBy(id = "address")
    private WebElement addressInput;

    @FindBy(id = "legalRepresentativeContact")
    private WebElement legalRepresentativeEmailInput;

    @FindBy(id = "legalRepresentative")
    private WebElement legalRepresentativeInput;

    @FindBy(id = "extCreditorId")
    private WebElement externalCreditorIdInput;

    // button
    @FindBy(css = "button.mc-next-save-btn.mc-button")
    private WebElement nextBtn;

    @FindBy(id = "entitySummaryCreateBtn")
    private WebElement saveEntityBtn;

    @FindBy(css = "p.error-message")
    private WebElement errorMessage;

    @FindBy(css = "button.mc-entity-close-btn")
    private WebElement buttonForClosePopup;

    final Table table = new Table(driver);
    public int numberOfEntities;

    public void createNewEntityWithRequiredFields() {
        uploadImageLnk.sendKeys(path);
        nameInput.sendKeys(NAME);
        abbreviationInput.sendKeys(ABBREVIATION);
        mailDomainInput.sendKeys(DOMAIN);
        webDomainInput.sendKeys(DOMAIN);

    }

    public void createNewEntityWithAllFields() {
        createNewEntityWithRequiredFields();
        descriptionInput.sendKeys(DESCRIPTION);
        ipWhitelistInput.sendKeys(IP_WHITELIST);
        phoneNumberInput.sendKeys(PHONE_NUMBER);
        vatNumberInput.sendKeys(VAT_NUMBER);
        faxNumberInput.sendKeys(FAX_NUMBER);
        impressumInput.sendKeys(IMPRESSUM);
        addressInput.sendKeys(ADDRESS);
        legalRepresentativeEmailInput.sendKeys(LEGAL_REPRESENTATIVE_EMAIL);
        legalRepresentativeInput.sendKeys(LEGAL_REPRESENTATIVE);
        externalCreditorIdInput.sendKeys(EXTERNAL_CREDITOR_ID);

    }

    public void filledFields(boolean onlyRequiredFieldsFilled) {
        if (onlyRequiredFieldsFilled)
            this.createNewEntityWithRequiredFields();
        else
            this.createNewEntityWithAllFields();
    }

    public void clickNextAndCreateEntity() throws Exception {
        invisibilityOfLoader();
        if (nextBtn.isEnabled()) {
            nextBtn.click();
            nextBtn.click();
            nextBtn.click();

            Allure.step("Click Create entity button");

            Methods.click(saveEntityBtn);

            //number of entites after creation in table
            int numberOfEntitiesAfterCreation = table.totalDisplayedElements();
            //if number of entities after creation - 1 is equal with number of creation before, new entity is created
            try {
                Assert.assertEquals(numberOfEntities, numberOfEntitiesAfterCreation - 1);
                Allure.step("Entity created!");
                //logout
                Methods.logout();
            } catch (AssertionError e) {
                Allure.step("Entity is not created!");
                throw new AssertionError("Entity is not created!");
            }

            //handling error messages
        } else if (errorMessage.getText().equals(Methods.getPropertyValue("EMPTY_REQUIRED_FIELDS"))) {
            Allure.step(Methods.getPropertyValue("EMPTY_REQUIRED_FIELDS"));
            throw new Exception(Methods.getPropertyValue("EMPTY_REQUIRED_FIELDS"));

        } else if (errorMessage.getText().equals(Methods.getPropertyValue("NOT_UNIQUE_NAME"))) {
            Allure.step(Methods.getPropertyValue("NOT_UNIQUE_NAME"));
            throw new Exception(Methods.getPropertyValue("NOT_UNIQUE_NAME"));

        } else if (errorMessage.getText().equals(Methods.getPropertyValue("NOT_UNIQUE_DOMAIN"))) {
            Allure.step(Methods.getPropertyValue("NOT_UNIQUE_DOMAIN"));
            throw new Exception(Methods.getPropertyValue("NOT_UNIQUE_DOMAIN"));

        } else {
            Allure.step(Methods.getPropertyValue("INVALID_DOMAIN_FORMAT"));
            throw new Exception(Methods.getPropertyValue("INVALID_DOMAIN_FORMAT"));


        }

    }

}
