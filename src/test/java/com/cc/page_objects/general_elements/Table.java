package com.cc.page_objects.general_elements;

import com.cc.helpers.Waiters;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class Table extends Waiters {

    //constructor
    public Table(WebDriver driver) throws Exception {
        super();
        PageFactory.initElements(driver,this);
    }

    //elements
    @FindBy(css = "tbody#cdk-drop-list-0 tr")
    public List<WebElement> elementsInTable;

    @FindBy(css = ".py-1>span.px-1")
    public WebElement totalDisplayedElements;

    @FindBy(css = ".mc-management-area>div>div>div.py-1")
    public WebElement tableTotalNubmerInfo;

    public int totalDisplayedElements(){
        invisibilityOfLoader();
        elementToAppear(tableTotalNubmerInfo);
        return Integer.parseInt(totalDisplayedElements.getText());
    }

    public int loadTableEntities(){
        elementToAppear(tableTotalNubmerInfo);
        return Integer.parseInt(totalDisplayedElements.getText());
    }

    //actions
    public boolean getTextFromElementsInTable(String text) {
        for (WebElement element : elementsInTable) {
            if (element.getText().contains(text)) {
                return true;
            }

        }
        return false;
    }
}
