package com.cc.page_objects.general_elements;

import com.cc.helpers.Methods;
import com.cc.helpers.Waiters;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class SideNavBar extends Waiters {

    //constructor
    public SideNavBar(WebDriver driver) throws Exception {
        super();
        PageFactory.initElements(driver, this);

    }

    //elements
    @FindBy(css = "li.sidebar-list-item a")
    public List<WebElement> modulesSideBar;

    @FindBy(css = "ul > li.mc-header-right-action.mc-header-logout > a")
    public WebElement logoutBtn;

    @FindBy(xpath = "//mc-menu//li//a[text()=' Entities ']")
    public WebElement entities;

    @FindBy(xpath = "//mc-menu//li//a[text()=' overview ']")
    public WebElement consumersOverview;


    public void getEntities(){
        driver.navigate().to(entities.getAttribute("href"));
    }

    public void clickConsumersOverviewInSideBar(){
        driver.navigate().to(consumersOverview.getAttribute("href"));
    }

    //actions
     public void clickOnModule(String moduleName) {
         for (WebElement element : modulesSideBar) {
             if (element.getText().equals(moduleName)) {
                 Methods.click(element);
             }
         }
     }


}
