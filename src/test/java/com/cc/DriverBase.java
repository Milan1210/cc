package com.cc;

import com.cc.config.DriverFactory;
import com.cc.helpers.URLs;
import com.cc.listeners.ScreenshotListener;
import io.qameta.allure.Allure;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Listeners(ScreenshotListener.class)
public class DriverBase {

    private static List<DriverFactory> webDriverThreadPool = Collections.synchronizedList(new ArrayList<DriverFactory>());
    private static ThreadLocal<DriverFactory> driverFactoryThread;
    public static WebDriver driver;

//    @BeforeSuite(alwaysRun = true)
//    public static void instantiateDriverObject() {
//        driverFactoryThread = ThreadLocal.withInitial(() -> {
//            DriverFactory driverFactory = new DriverFactory();
//            webDriverThreadPool.add(driverFactory);
//            return driverFactory;
//        });
//    }

    public static WebDriver getDriver() throws Exception {
        System.setProperty("webdriver.chrome.driver","src/driver/chromedriver.exe");
        return driver = new ChromeDriver();
//        return driverFactoryThread.get().getDriver();
    }

    @AfterMethod(alwaysRun = true)
    public static void clearCookies() {
        try {
            driverFactoryThread.get().getStoredDriver().manage().deleteAllCookies();
        } catch (Exception ignored) {
            System.out.println("Unable to clear cookies, driver object is not viable...");
        }
    }

    @AfterSuite(alwaysRun = true)
    public static void closeDriverObjects() {
        for (DriverFactory driverFactory : webDriverThreadPool) {
            driverFactory.quitDriver();

        }
    }

    @BeforeClass
    public static void goToUrl() throws Exception {
        driver = getDriver();
        Allure.step("Go to url: " + URLs.LOGIN_PAGE);
        driver.manage().window().maximize();
        driver.get(URLs.LOGIN_PAGE);
        //dodat wdrivermanager

    }
}